from django import forms

from .models import Saran

class SaranForm(forms.ModelForm):
    class Meta:
        model = Saran
        fields = [
            'nama',
            'saran',
        ]

        widgets = {
            'nama' : forms.Textarea (
                attrs = {
                    'class' : 'form-control',
                    'placeholder' : 'Masukan nama ...',
                    'rows' : '1',
                }
            ),
            'saran' : forms.Textarea (
                attrs = {
                    'class' : 'form-control',
                    'placeholder' : 'Berikan saranmu ...',
                    'rows' : '4',
                }
            ),
        }
