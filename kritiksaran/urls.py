from django.urls import path

from . import views

app_name = 'kritiksaran'

urlpatterns = [
    path('', views.home, name='home'),
]
