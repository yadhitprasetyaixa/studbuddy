from django.test import LiveServerTestCase, TestCase, tag
from django.urls import reverse
from selenium import webdriver
from django.urls import resolve
from .views import home
from .models import Saran
from django.utils import timezone
from .forms import SaranForm
# Create your tests here.

class kritiksaranTest (TestCase):
    def test_url_exist (self):
        response = self.client.get('/saran/')
        self.assertEqual(response.status_code, 200)
        # You can also use path names instead of explicit paths.
        response = self.client.get(reverse('kritiksaran:home'))
        self.assertEqual(response.status_code, 200)

    def test_landing_page_using_index_func(self):
        found = resolve('/saran/')
        self.assertEqual(found.func, home)

    def test_landing_page_uses_template(self):
        response = self.client.get('/saran/')
        self.assertTemplateUsed(response, 'kritiksaran/home.html')

    def test_model_can_create_new_saran(self):
        Saran.objects.create(
            nama = 'bejo',
            saran = 'Minggu ini santuy parah',
            time = timezone.now()
        )

        count_all_saran = Saran.objects.all().count()
        self.assertEqual(count_all_saran, 1)

    def test_model_str_function(self):
        obj = Saran.objects.create(
            nama = 'bejo',
            saran = 'Minggu ini santuy parah',
            time = timezone.now()
        )

        self.assertTrue(isinstance(obj, Saran))
        self.assertEqual(obj.__str__(), obj.saran)

    def test_valid_form(self):
        obj = Saran.objects.create(
            nama = 'bejo',
            saran = 'Minggu ini santuy parah',
            time = timezone.now()
        )
        data = {
            'nama' : obj.nama,
            'saran' : obj.saran,
            'time' : obj.time,
        }
        form = SaranForm(data=data)
        self.assertTrue(form.is_valid())

    def test_invalid_form(self):
        obj = Saran.objects.create(
            nama = '',
            saran = '',
            time = 123
        )
        data = {
            'nama': obj.nama,
            'saran' : obj.saran,
            'time' : obj.time,
        }
        form = SaranForm(data=data)
        self.assertFalse(form.is_valid())


    