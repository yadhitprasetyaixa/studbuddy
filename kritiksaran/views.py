from django.shortcuts import render, redirect
from .models import Saran
from .forms import SaranForm


# Create your views here.
def home(request):
    saran = Saran.objects.all().order_by('-time')
    saran_form = SaranForm(request.POST or None)
    ukuran = Saran.objects.all().count()
    if request.method == "POST":
        if saran_form.is_valid():
            saran_form.save()

            return redirect('/saran/')

 
    context = {
        'sarans' : saran,
        'form_saran' : saran_form,
        'size' : ukuran
    }
    return render(request, 'kritiksaran/home.html',context)