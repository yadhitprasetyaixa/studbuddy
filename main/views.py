from django.http import HttpResponse
from django.shortcuts import render

def home(request):
    response = {}
    return render(request,"main/home2.html",response)

def aboutUs(request):
    response = {}
    return render(request,"main/about us.html", response)

