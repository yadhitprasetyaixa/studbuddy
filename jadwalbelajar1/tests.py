from django.test import TestCase, Client
from django.urls import resolve, reverse
from .models import *
from . import views

# Create your tests here.
class UnitTestForJadwalBelajar(TestCase):
    def test_response_page(self):
        response = Client().get('/jadwalbelajar/')
        self.assertEqual(response.status_code, 200)
    
    def test_template_used_for_begin(self):
        response = Client().get('/jadwalbelajar/')
        self.assertTemplateUsed(response, 'jadwalbelajar.html')
    
    def test_str(self):
        nama = modelJadwal.objects.create(nama = 'joni', matkul = 'PPW', waktu = '12:00', link = 'https://gitlab.com/yadhitprasetyaixa/studbuddy')
        result = modelJadwal.objects.get(id=1)
        self.assertEqual(str(nama), 'joni')
    
    def test_func(self):
        found = resolve('/jadwalbelajar/')
        self.assertEqual(found.func, views.jadwalbelajar)
    
    def test_POST_delete(self):
        self.client = Client()
        self.c = modelJadwal.objects.create(nama = 'joni', matkul = 'PPW', waktu = '12:00', link = 'https://gitlab.com/yadhitprasetyaixa/studbuddy')
        self.delete = reverse('jadwalbelajar1:delete_jadwal', args=[self.c.id])
        response = self.client.post(self.delete, {
            'nama': 'joni',
            'matkul': 'PPW',
            'waktu': '12:00',
            'link': 'https://gitlab.com/yadhitprasetyaixa/studbuddy',
        }, follow=True)
        self.assertEqual(response.status_code, 200)