from django.urls import path
from . import views

app_name = 'jadwalbelajar1'

urlpatterns = [
    path('', views.jadwalbelajar, name='jadwalbelajar'),
    path('post', views.post_jadwal, name='post_jadwal'),
    path('delete/<int:id>', views.delete_jadwal, name='delete_jadwal'),
]