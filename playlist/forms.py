from django import forms
from .models import Song

class Song_Form(forms.ModelForm):
    class Meta:
        model = Song
        fields = ['kontributor', 'judul', 'genre', 'link']
    error_messages = {
        'required' : 'Please Type'
    }