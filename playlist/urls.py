from django.urls import path
from django.conf.urls import url
from .views import songlist, addsong, savesong, delsong, filterbycont, filterbygenre

app_name = 'playlist'


urlpatterns = [
    path('', songlist, name="playlist"),
    path('addsong/', addsong),
    path('addsong/save', savesong),
    path('delete/<int:kp>/', delsong, name="delsong"),
    path('filter/kontributor/<str:name>', filterbycont, name="filterbycont"),
    path('filter/genre/<str:genre>', filterbygenre, name="filterbygenre")

]
