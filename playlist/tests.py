from django.test import TestCase, Client
from .models import Song

# Create your tests here.
class TestActivity(TestCase):
        def test_url_playlist(self):
            response = Client().get('/playlist/')
            self.assertEquals(response.status_code, 200)

        def test_template_playlist(self):
            response = Client().get('/playlist/')
            self.assertTemplateUsed(response, 'playlist.html')

        def test_elemen_playlist(self):
            response = Client().get('/playlist/')
            returned_html = response.content.decode('utf8')
            self.assertIn('Playlist Lagu', returned_html)
            self.assertIn('Tambahkan lagu', returned_html)
            self.assertIn('Kontributor', returned_html)
            self.assertIn('Judul', returned_html)
            self.assertIn('Genre', returned_html)
            self.assertIn('Link', returned_html)
        
        def test_url_addsong(self):
            response = Client().get('/playlist/addsong/')
            self.assertEquals(response.status_code, 200)
        
        def test_template_addsong(self):
            response = Client().get('/playlist/addsong/')
            self.assertTemplateUsed(response, 'addsong.html')
        
        def test_create_song(self):
            new_song = Song.objects.create(kontributor='gw', judul='apaya', genre='pop', link='https://google.com/')
            count = Song.objects.all().count()
            self.assertEqual(count, 1)

        def test_url_filterbycont(self):
            song = Song.objects.create(kontributor='gw', judul='apaya', genre='rock', link='https://bing.com/')
            response = Client().get('/playlist/filter/kontributor/gw')
            self.assertEquals(response.status_code, 200)

        def test_template_filterbycont(self):
            song = Song.objects.create(kontributor='gw', judul='apaya', genre='rock', link='https://bing.com/')
            response = Client().get('/playlist/filter/kontributor/gw')
            self.assertTemplateUsed(response, 'filterbycont.html')
        
        def test_url_filterbygenre(self):
            song = Song.objects.create(kontributor='gw', judul='apaya', genre='rock', link='https://bing.com/')
            response = Client().get('/playlist/filter/genre/rock')
            self.assertEquals(response.status_code, 200)

        def test_template_filterbygenre(self):
            song = Song.objects.create(kontributor='gw', judul='apaya', genre='rock', link='https://bing.com/')
            response = Client().get('/playlist/filter/genre/rock')
            self.assertTemplateUsed(response, 'filterbygenre.html')