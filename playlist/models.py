from django.db import models

# Create your models here.
class Song(models.Model):
    kontributor = models.CharField(max_length=50)
    judul = models.CharField(max_length=100)
    genre = models.CharField(max_length=50)
    link = models.CharField(max_length=300)
