# Generated by Django 3.1.3 on 2020-11-11 08:52

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Contributor',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Song',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('judul', models.CharField(max_length=100)),
                ('genre', models.CharField(max_length=50)),
                ('link', models.CharField(max_length=300)),
                ('contrib', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='playlist.contributor')),
            ],
        ),
    ]
