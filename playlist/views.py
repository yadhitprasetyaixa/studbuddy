from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import Song
from .forms import Song_Form

# Create your views here.
def songlist(request):
    response = {'songlist' : Song.objects.all()}
    return render(request, 'playlist.html', response)

def addsong(request):
    response = {'s_form' : Song_Form}
    return render(request, 'addsong.html', response)

def savesong(request):
    song = Song.objects.all()
    form = Song_Form(request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            form.save()
            # song.create(
            #     kontributor=form.cleaned_data.get('kontributor'),
            #     judul=form.cleaned_data.get('judul'),
            #     genre=form.cleaned_data.get('genre'),
            #     link=form.cleaned_data.get('link')
            # )
    return HttpResponseRedirect('/playlist')
        
def delsong(request, kp):
    sg = Song.objects.get(id = kp)
    sg.delete()
    return HttpResponseRedirect('/playlist/')

def filterbycont(request, name):
    response = {'songs' : Song.objects.filter(kontributor=name),
                'cont' : name}
    return render(request, 'filterbycont.html', response)

def filterbygenre(request, genre):
    response = {'songs' : Song.objects.filter(genre=genre),
                'gen' : genre}
    return render(request, 'filterbygenre.html', response)