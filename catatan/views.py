from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import modelCatatan
from .forms import catatanForm
from django.db.models import F

# Create your views here.

def post_catatan(request):
    if request.method == 'POST':
        response_data = {}
        form = catatanForm(request.POST or None, request.FILES or None)
        if form.is_valid():
            response_data['nama'] = request.POST['nama']
            response_data['fakultas'] = request.POST['fakultas']
            response_data['matkul'] = request.POST['matkul']
            response_data['deskripsi'] = request.POST['deskripsi']
            response_data['link'] = request.POST['link']

            data_catatan = modelCatatan(nama = response_data['nama'],
                                        fakultas = response_data['fakultas'],
                                        matkul = response_data['matkul'],
                                        deskripsi = response_data['deskripsi'],
                                        link = response_data['link'],    
                                        )
            data_catatan.save()
            return redirect('/catatan/#catatanTerdaftar')
        else:
            return redirect('/catatan')
    else:
        return redirect('/catatan')

def catatan(request):
    form_tambah = catatanForm()
    data = modelCatatan.objects.all()
    response ={
        'form_tambah' : form_tambah,
        'data' : data,

    }
    return render(request, 'catatan.html', response)

def detail_catatan(request, nama_cat):
    data = modelCatatan.objects.filter(nama = nama_cat)
    datas = modelCatatan.objects.all()
    response = {
        'data' : data,
        'datas' : datas,
    }
    return render(request, 'catatan-2.html', response)

def delete_catatan(request, nama_cat):
    print(nama_cat)
    data = modelCatatan.objects.filter(nama = nama_cat)
    data.delete()
    return redirect('/catatan')