from django import forms

class catatanForm(forms.Form):
    error_message = {
        'required' : 'This field is required'
    }
    nama = forms.CharField(widget=forms.TextInput(attrs={'class' : 'form-control', 'placeholder' : 'Masukkan Nama', 'style':'border-radius: 20px; text-align: center;'}), label='Nama ', max_length=50, required=True)
    fakultas = forms.CharField(widget=forms.TextInput(attrs={'class' : 'form-control','placeholder' : 'Masukkan Fakultas (Universitas)', 'style':'border-radius: 20px; text-align: center;'}), label='Fakultas ', max_length=100, required=True)
    matkul = forms.CharField(widget=forms.TextInput(attrs={'class' : 'form-control','placeholder' : 'Masukkan Nama Mata Kuliah', 'style':'border-radius: 20px; text-align: center;'}), label='Mata Kuliah ', max_length=50, required=True)
    deskripsi = forms.CharField(widget=forms.Textarea(attrs={'class' : 'form-control','placeholder' : 'Masukkan Deskripsi', 'style':'border-radius: 20px; text-align: center;'}), label="Deskripsi ", required=True)
    link = forms.URLField(widget=forms.TextInput(attrs={'class' : 'form-control','placeholder' : 'http://www.link-web.com', 'style':'border-radius: 20px; text-align: center;'}), label='Link Catatan ', required =True)