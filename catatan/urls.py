from django.urls import path
from . import views

urlpatterns = [
    path('', views.catatan),
    path('post', views.post_catatan),
    path('<str:nama_cat>', views.detail_catatan),
    path('delete/<str:nama_cat>', views.delete_catatan),
]