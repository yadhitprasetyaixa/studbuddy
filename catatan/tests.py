from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .models import modelCatatan
from . import views

# Create your tests here.

class UnitTestForCatatan(TestCase):
    def test_response_page(self):
        response = Client().get('/catatan/')
        self.assertEqual(response.status_code, 200)
    
    def test_template_used_for_begin(self):
        response = Client().get('/catatan/')
        self.assertTemplateUsed(response, 'catatan.html')

    def test_tulisan_atas_page(self):
        response = Client().get('/catatan/')
        isi_html_atas = response.content.decode('utf8')
        self.assertIn("Tambah Catatan", isi_html_atas)

    def test_isi_dalam_page(self):
        response = Client().get('/catatan/')
        isi_dalam = response.content.decode('utf8')
        self.assertIn('<button type="submit" class="btn" style="border-radius: 20px; background-color: #f8c246;" id="bton">Tambah ke Catatan</button>', isi_dalam)

    def test_template_used_for_next(self):
        response = Client().get('/catatan/<str:nama_cat>')
        self.assertTemplateUsed(response, 'catatan-2.html')
    
    def test_isi_dalam_page_two(self):
        response = Client().get('/catatan/<str:nama_cat>')
        isi_page = response.content.decode('utf8')
        self.assertIn('Nama', isi_page)
        self.assertIn('Nama Matkul', isi_page)

    def test_func_page(self):
        found = resolve('/catatan/')
        self.assertEqual(found.func, views.catatan)

    def test_save_catatan_not_Valid(self):
        Client().post('/catatan/<str:nama_cat>', data={'nama_cat': 'hasbi'})
        jumlah = modelCatatan.objects.filter(nama='hasbi').count()
        self.assertEqual(jumlah, 0)
    
    def test_func_page_two(self):
        found = resolve('/catatan/<str:nama_cat>')
        self.assertEqual(found.func, views.detail_catatan)

    