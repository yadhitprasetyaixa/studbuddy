from django.db import models

# Create your models here.
class modelCatatan(models.Model):
    nama = models.CharField(max_length=50)
    fakultas = models.CharField(max_length=100)
    matkul = models.CharField(max_length=50)
    deskripsi = models.TextField()
    link = models.URLField()

    def __str__(self):
        return self.nama