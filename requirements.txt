appdirs==1.4.4
distlib==0.3.1
dj-database-url==0.5.0
Django==3.1.2
filelock==3.0.12
six==1.15.0
virtualenv==20.0.31
whitenoise==5.2.0
asgiref==3.2.10
certifi==2020.11.8
chardet==3.0.4
coverage==5.3
gunicorn==20.0.4
idna==2.10
psycopg2-binary==2.8.6
pytz==2020.4
requests==2.25.0
selenium==3.141.0
sqlparse==0.4.1
urllib3==1.26.1
astroid==2.4.2
colorama==0.4.3
django-crispy-forms==1.9.2
django-environ==0.4.
isort==5.5.2
lazy-object-proxy==1.4.3
mccabe==0.6.1
pylint==2.6.0
six==1.15.0
toml==0.10.1
typed-ast==1.4.1
wrapt==1.12.1