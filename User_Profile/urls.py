from django.urls import path
from . import views

app_name = 'User_Profile'

urlpatterns = [
    path('', views.profile, name="home"),
]