from django.shortcuts import render

# Create your views here.
def profile(request):
    response = {}
    return render(request,"user_profile.html")